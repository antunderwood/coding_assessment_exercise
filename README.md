# Technical Assessment Exercise

## 1 Variant Calling
Imagine you have been given a pair of fastq files (`sample1.R1.fastq.gz` and `sample1.R2.fastq.gz`) generated by short read sequencing (2x150bp) of a bacterial genome. It is determined that the complete chromosome sequence of a close reference strain to the sequenced genome can be found in the file `reference.fas`.

Describe the steps and the commands you would take to map the reads to the reference, call variants and filter so that low quality variants are removed.

## 2 Data parsing
The software Ariba is one piece of software that can be used to predict the presence of loci responsible for antimicrobial resistance in bacterial genomes.
After running the software on whole genome data from individual isolates the summary command can be used to summarize the presence and absence of loci across all the isolates. The output is described on this wiki page
Given a
 * [Ariba output file](ariba_amr_output.csv)
 * [NCBI acquired gene metadata file](ncbi_acquired_genes_metadata.csv)

Write code in the programming language of your choice that will parse this data to find those samples that contain loci responsible for conferring cephalosporin or carbapenem resistance.

These are defined as samples that
* contain a locus that has yes or yes_nonunique in the locus column with the `.assembled` suffix
* a coverage of at least 10 in the locus column with the `.ctg_cov` suffix
* the reference sequence described in the column with the `.ref_seq` suffix beginning `NG_*` is associated with a gene described in the NCBI metadata file that is associated with the `CEPHALOSPORIN` or `CARBAPENEM` subclass respectively 


Please upload your code to a public git repository such as github or gitlab.

## 3 Nextflow workflows
Nextflow is a workflow management tool that allows parallel processing of multiple samples. See [documentation](https://www.nextflow.io/docs/latest/index.html).

The aim of this exercise is to add another process to an existing nextflow workflow so that two pieces of software for determining Salmonella serotype are used to analyse 3 genome assembly files from Salmonella:
 1. [seqsero](https://github.com/denglab/SeqSero2)
 2. [SISTR](https://github.com/phac-nml/sistr_cmd)

#### Instructions
* On a UNIX machine you have access to please install
    1. [miniconda](https://docs.conda.io/projects/conda/en/latest/user-guide)
    2. [nextflow]( https://www.nextflow.io) Please note this will require a Java run time (JRE) to be installed

* Activate the conda base environment and use the conda CLI to install seqsero2 and SISTR.
* Download [nextflow.config](exercise_3/nextflow.config) and starting nextflow workflow file [main.nf](exercise_3/main.nf)
* Download the fasta input files from [here](exercise_3/fastas)

* Starting with the main.nf script as a starting point, add another process to the workflow to run SISTR.

    * Below is an example of the structure of a Nextflow process.
      
      ![nextflow](exercise_3/nextflow_process.png)

    * Here is the basic structure of a SISTR command used to determine serotype from an asembly file in fasta format.
      ```
      sistr --qc -vv --alleles-output allele-results.json --novel-alleles novel-alleles.fasta --cgmlst-profiles cgmlst-profiles.csv -f tab -o sistr-output.tab assembly.fasta
      ```

* Concatenate and merge the text output files from both processes by sample id *and write a brief report presenting a comparison of the outputs. Please send the report as a Google Slides presentation. 



**Please Note** The sequence data file provided are property of AGROSAVIA. It is only approved for use as training or test purposes.


